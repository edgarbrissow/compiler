/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

/**
 *
 * @author Edgar
 */
public class GUIMessages {
    
    
     GUIMessages(String op) {
        if (op.equals("BR")) {
            OVERRIDE = "O arquivo já existe, sobrescrever?";
            EXIST_FILE = "Arquivo existente";
            DIALOG_EXIT = "Sair";
            DIALOG_CONFIRM = "Confirmação";
            DIALOG_SAVE_EXIT = "Salvar antes de sair?";
            DIALOG_SAVE_OPEN = "Salvar antes de abrir um arquivo?";
            DIALOG_ERROR = "ERRO";

            // Aplication
            APP_NAME = "COMPILADOR";
            APP_FILE_BUTTON_TP = "Novo arquivo";
            APP_OPEN_BUTTON_TP = "Abre o arquivo";
            APP_SAVE_BUTTON_TP = "Salva o arquivo";
            APP_CUT_BUTTON_TP = ("Recorta o texto selecionado");
            APP_COPY_BUTTON_TP = ("Copia o texto selecionado");
            APP_PASTE_BUTTON_TP = ("Cola o conteúdo");
            APP_FILE_MENU = ("Arquivo");
            APP_NEW_MENU_I = ("Novo");
            APP_OPEN_MENU_I = ("Abrir");
            APP_SAVE_MENU_I = ("Salvar");
            APP_SAVE_AS_MENU_I = ("Salvar como");
            APP_EXIT_MENU_I = ("Sair");
            APP_EDIT_MENU = ("Edição");
            APP_COPY_MENU_I = ("Copiar");
            APP_PASTE_MENU_I = ("Colar");
            APP_CUT_MENU_I = ("Recortar");
            APP_COMPILE_MENU = ("Compilação");
            APP_COMPILE_MENU_I = ("Compilar");
            APP_EXECUTE_MENU_I = ("Executar");

            // Aplication Messages
            APP_MESSAGE_NOTHING_COMPILE = "Nada para compilar.";
            APP_MESSAGE_NO_LINE = "Nenhuma linha selecionada.";
            APP_MESSAGE_YES = "Sim";
            APP_MESSAGE_NO = "Não";
            APP_MESSAGE_CANCEL = "Cancelar";

        }
    }

    static String OVERRIDE = "The file exists, overwrite?";
    static String EXIST_FILE = "Existing file";
    static String DIALOG_EXIT = "Exit";
    static String DIALOG_SAVE_EXIT = "Save before exit?";
    static String DIALOG_SAVE_OPEN = "Save before open file?";
    static String DIALOG_ERROR = "ERROR";
    static String DIALOG_CONFIRM = "Confirm";

    // Aplication
    static String APP_NAME = "COMPILADOR";
    static String APP_FILE_BUTTON_TP = "New";
    static String APP_OPEN_BUTTON_TP = "Open";
    static String APP_SAVE_BUTTON_TP = "Save";
    static String APP_CUT_BUTTON_TP = ("Cut");
    static String APP_COPY_BUTTON_TP = ("Copy");
    static String APP_PASTE_BUTTON_TP = ("Paste");
    static String APP_FILE_MENU = ("File");
    static String APP_NEW_MENU_I = ("New");
    static String APP_OPEN_MENU_I = ("Open");
    static String APP_SAVE_MENU_I = ("Save");
    static String APP_SAVE_AS_MENU_I = ("Save as");
    static String APP_EXIT_MENU_I = ("Exit");
    static String APP_EDIT_MENU = ("Edit");
    static String APP_COPY_MENU_I = ("Copy");
    static String APP_PASTE_MENU_I = ("Paste");
    static String APP_CUT_MENU_I = ("Cut");
    static String APP_COMPILE_MENU = ("Compile");
    static String APP_COMPILE_MENU_I = ("Compile");
    static String APP_EXECUTE_MENU_I = ("Execute");
    

    // Aplication Messages
    static String APP_MESSAGE_NOTHING_COMPILE = "Nothing to compile.";
    static String APP_MESSAGE_NO_LINE = "No line selected.";
    static String APP_MESSAGE_YES = "Yes";
    static String APP_MESSAGE_NO = "No";
    static String APP_MESSAGE_CANCEL = "Cancel";

   

}
