/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package gui;

import file.CompilerFileFilter;
import java.awt.Cursor;
import java.awt.Point;
import java.awt.Toolkit;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.io.File;
import javax.swing.JFileChooser;
import javax.swing.JOptionPane;
import javax.swing.event.CaretEvent;
import javax.swing.event.CaretListener;
import javax.swing.text.Caret;

/**
 *
 * @author Edgar
 */
public class MainWidow extends javax.swing.JFrame {

    private file.CompilerFile file;
    private String name = GUIMessages.APP_NAME;
    private String currentDirectory = "";

    /**
     * Creates new form MainWidow
     */
    public MainWidow() {
        initComponents();
        this.file = new file.CompilerFile();
        new GUIMessages("BR");
        setInterfaceNames();
        updateTitleFilename(this.file.getName(), this.file.getExtension());

    }

    private void setInterfaceNames() {
        setTitle(GUIMessages.APP_NAME);
        fileButton.setToolTipText(GUIMessages.APP_FILE_BUTTON_TP);
        openButton.setToolTipText(GUIMessages.APP_OPEN_BUTTON_TP);
        saveButton.setToolTipText(GUIMessages.APP_SAVE_BUTTON_TP);
        cutButton.setToolTipText(GUIMessages.APP_CUT_BUTTON_TP);
        copyButton.setToolTipText(GUIMessages.APP_COPY_BUTTON_TP);
        pasteButton.setToolTipText(GUIMessages.APP_PASTE_BUTTON_TP);
        lineLabel.setText(GUIMessages.APP_MESSAGE_NO_LINE);
        fileMenu.setText(GUIMessages.APP_FILE_MENU);
        newMenuItem.setText(GUIMessages.APP_NEW_MENU_I);
        openMenuItem.setText(GUIMessages.APP_OPEN_MENU_I);
        saveMenuItem.setText(GUIMessages.APP_SAVE_MENU_I);
        saveAsMenuItem.setText(GUIMessages.APP_SAVE_AS_MENU_I);
        exitMenuItem.setText(GUIMessages.APP_EXIT_MENU_I);
        editMenu.setText(GUIMessages.APP_EDIT_MENU);
        copyMenuItem.setText(GUIMessages.APP_COPY_MENU_I);
        pasteMenuItem.setText(GUIMessages.APP_PASTE_MENU_I);
        cutMenuItem.setText(GUIMessages.APP_CUT_MENU_I);
        compileMenu.setText(GUIMessages.APP_COMPILE_MENU);
        compileMenuItem.setText(GUIMessages.APP_COMPILE_MENU_I);
        executeMenuItem.setText(GUIMessages.APP_EXECUTE_MENU_I);
    }

    private void showErrorDialog(String error) {
        JOptionPane.showMessageDialog(this, GUIMessages.DIALOG_ERROR, error, JOptionPane.OK_OPTION);
    }

    private int showConfirmDialog(String confirm) {
        return JOptionPane.showConfirmDialog(this, GUIMessages.DIALOG_CONFIRM, confirm, JOptionPane.YES_NO_CANCEL_OPTION);
    }

    private JFileChooser getSaveFileChooser() {
        return new JFileChooser() {
            @Override
            public void approveSelection() {
                File f = getSelectedFile();
                if (f.exists() && getDialogType() == SAVE_DIALOG) {
                    int result = JOptionPane.showConfirmDialog(this, GUIMessages.OVERRIDE, GUIMessages.EXIST_FILE, JOptionPane.YES_NO_CANCEL_OPTION);
                    switch (result) {
                        case JOptionPane.YES_OPTION:
                            super.approveSelection();
                            return;
                        case JOptionPane.NO_OPTION:
                            return;
                        case JOptionPane.CLOSED_OPTION:
                            return;
                        case JOptionPane.CANCEL_OPTION:
                            cancelSelection();
                            return;
                    }
                }
                super.approveSelection();
            }
        };
    }

    private Boolean saveAsFile() {
        JFileChooser saveFileChooser = getSaveFileChooser();

        saveFileChooser.setSelectedFile(this.file.getFile());
        System.out.println(JFileChooser.SAVE_DIALOG);
        saveFileChooser.setFileFilter(new CompilerFileFilter(this.file));
        int returnVal = saveFileChooser.showSaveDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            java.io.File file = saveFileChooser.getSelectedFile();
            String filename = saveFileChooser.getName(file);
            this.file.setName(filename);
            this.file.setPath(file.getPath());
            this.file.save();
            updateTitleFilename(this.file.getName(), this.file.getExtension());
            return true;
        } else {
            return false;
        }
//        while(this.file.save());
    }

    private void openFile() {
        JFileChooser saveFileChooser = getSaveFileChooser();
        saveFileChooser.setSelectedFile(this.file.getFile());
        System.out.println(JFileChooser.SAVE_DIALOG);
        saveFileChooser.setFileFilter(new CompilerFileFilter(this.file));
        int returnVal = saveFileChooser.showOpenDialog(this);
        if (returnVal == JFileChooser.APPROVE_OPTION) {
            java.io.File file = saveFileChooser.getSelectedFile();
            String filename = saveFileChooser.getName(file);
            this.file.setName(filename);
            this.file.setPath(file.getPath());
            try {
                String s = this.file.load();
                editTextArea.setText(s);
                updateContent();
                this.file.setSaved(true);
                updateTitleFilename(this.file.getName(), this.file.getExtension());
            } catch (Exception e) {
            }

        }

    }
    
    private void openFileAction(){
     if (this.file.isSaved()) {
            openFile();
        } else {
            if (this.file.isOpened()) {
                int s = showConfirmDialog(GUIMessages.DIALOG_SAVE_OPEN);
                switch (s) {
                    case 0:
                        this.file.save();
                        openFile();
                        break;
                    case 1:
                        openFile();
                        break;
                    default:
                        clear();
                        break;
                }

            } else {
                saveAsFile();
                openFile();
            }

        }
    
    }

    private void compile() {
        if (this.file.getContent().isEmpty()) {
            outTextArea.setText(GUIMessages.APP_MESSAGE_NOTHING_COMPILE);
        } else {

            //TODO compila e retorna string
            outTextArea.setText(name);
        }

    }

    public void updateTitleFilename(String filename, String extension) {
        this.setTitle(name + " - [" + filename + "." + extension + "]");
    }

    private void clear() {
        outTextArea.setText("");
        editTextArea.setText("");
        file.clear();
    }
    
    private void newFile(){
        clear();
        updateTitleFilename(this.file.getName(), this.file.getExtension());
    }
    
    private void saveFile(){
        if (this.file.isOpened()) {
            this.file.save();
        } else {
            saveAsFile();
        }
    }
    
    private void copyToClipboard(){
        editTextArea.copy();
//        String selected = editTextArea.getSelectedText();
//        StringSelection stringSelection = new StringSelection(selected);
//        Clipboard clip = Toolkit.getDefaultToolkit().getSystemClipboard();
//        clip.setContents(stringSelection, null);
    }
    
    private void pasteFromClipboard(){
        editTextArea.paste();
    }
    
    private void cutToClipboard(){
        String selected = editTextArea.getSelectedText();
        editTextArea.cut();
    }

    private void updateContent() {
        file.addContent(editTextArea.getText());
    }

    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        toolBar = new javax.swing.JToolBar();
        fileButton = new javax.swing.JButton();
        openButton = new javax.swing.JButton();
        saveButton = new javax.swing.JButton();
        toolBarSeparator = new javax.swing.JToolBar.Separator();
        cutButton = new javax.swing.JButton();
        copyButton = new javax.swing.JButton();
        pasteButton = new javax.swing.JButton();
        editPanel = new javax.swing.JScrollPane();
        editTextArea = new javax.swing.JTextArea();
        outPanel = new javax.swing.JScrollPane();
        outTextArea = new javax.swing.JTextArea();
        footPanel = new javax.swing.JPanel();
        lineLabel = new javax.swing.JLabel();
        menuBar = new javax.swing.JMenuBar();
        fileMenu = new javax.swing.JMenu();
        newMenuItem = new javax.swing.JMenuItem();
        openMenuItem = new javax.swing.JMenuItem();
        saveMenuItem = new javax.swing.JMenuItem();
        saveAsMenuItem = new javax.swing.JMenuItem();
        exitMenuItem = new javax.swing.JMenuItem();
        editMenu = new javax.swing.JMenu();
        copyMenuItem = new javax.swing.JMenuItem();
        pasteMenuItem = new javax.swing.JMenuItem();
        cutMenuItem = new javax.swing.JMenuItem();
        compileMenu = new javax.swing.JMenu();
        compileMenuItem = new javax.swing.JMenuItem();
        executeMenuItem = new javax.swing.JMenuItem();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("COMPILADOR");
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
        });

        toolBar.setRollover(true);

        fileButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/file_icon.png"))); // NOI18N
        fileButton.setToolTipText("New");
        fileButton.setBorderPainted(false);
        fileButton.setContentAreaFilled(false);
        fileButton.setFocusable(false);
        fileButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        fileButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        fileButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                fileButtonActionPerformed(evt);
            }
        });
        toolBar.add(fileButton);

        openButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/file_open.png"))); // NOI18N
        openButton.setToolTipText("Open");
        openButton.setBorderPainted(false);
        openButton.setContentAreaFilled(false);
        openButton.setFocusable(false);
        openButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        openButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        openButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openButtonActionPerformed(evt);
            }
        });
        toolBar.add(openButton);

        saveButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/file_save.png"))); // NOI18N
        saveButton.setToolTipText("Save");
        saveButton.setBorderPainted(false);
        saveButton.setContentAreaFilled(false);
        saveButton.setFocusable(false);
        saveButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        saveButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        saveButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveButtonActionPerformed(evt);
            }
        });
        toolBar.add(saveButton);
        toolBar.add(toolBarSeparator);

        cutButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/editor_cut.png"))); // NOI18N
        cutButton.setToolTipText("Cut");
        cutButton.setBorderPainted(false);
        cutButton.setContentAreaFilled(false);
        cutButton.setFocusable(false);
        cutButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        cutButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        cutButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cutButtonActionPerformed(evt);
            }
        });
        toolBar.add(cutButton);

        copyButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/editor_copy.png"))); // NOI18N
        copyButton.setToolTipText("Copy");
        copyButton.setBorderPainted(false);
        copyButton.setContentAreaFilled(false);
        copyButton.setFocusable(false);
        copyButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        copyButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        copyButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyButtonActionPerformed(evt);
            }
        });
        toolBar.add(copyButton);

        pasteButton.setIcon(new javax.swing.ImageIcon(getClass().getResource("/assets/icons/editor_paste.png"))); // NOI18N
        pasteButton.setToolTipText("Paste");
        pasteButton.setBorderPainted(false);
        pasteButton.setContentAreaFilled(false);
        pasteButton.setFocusable(false);
        pasteButton.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        pasteButton.setVerticalTextPosition(javax.swing.SwingConstants.BOTTOM);
        pasteButton.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pasteButtonActionPerformed(evt);
            }
        });
        toolBar.add(pasteButton);

        editTextArea.setColumns(20);
        editTextArea.setRows(5);
        editTextArea.addFocusListener(new java.awt.event.FocusAdapter() {
            public void focusGained(java.awt.event.FocusEvent evt) {
                editTextAreaFocusGained(evt);
            }
            public void focusLost(java.awt.event.FocusEvent evt) {
                editTextAreaFocusLost(evt);
            }
        });
        editTextArea.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                editTextAreaKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                editTextAreaKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                editTextAreaKeyTyped(evt);
            }
        });
        editPanel.setViewportView(editTextArea);

        outTextArea.setEditable(false);
        outTextArea.setColumns(20);
        outTextArea.setRows(5);
        outPanel.setViewportView(outTextArea);

        footPanel.setBorder(javax.swing.BorderFactory.createTitledBorder(""));

        lineLabel.setText("No line selected");

        javax.swing.GroupLayout footPanelLayout = new javax.swing.GroupLayout(footPanel);
        footPanel.setLayout(footPanelLayout);
        footPanelLayout.setHorizontalGroup(
            footPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(footPanelLayout.createSequentialGroup()
                .addContainerGap()
                .addComponent(lineLabel)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        footPanelLayout.setVerticalGroup(
            footPanelLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(lineLabel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, 25, Short.MAX_VALUE)
        );

        fileMenu.setText("File");

        newMenuItem.setText("New");
        newMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                newMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(newMenuItem);

        openMenuItem.setText("Open");
        openMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                openMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(openMenuItem);

        saveMenuItem.setText("Save");
        saveMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveMenuItem);

        saveAsMenuItem.setText("Save as");
        saveAsMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                saveAsMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(saveAsMenuItem);

        exitMenuItem.setText("Exit");
        exitMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                exitMenuItemActionPerformed(evt);
            }
        });
        fileMenu.add(exitMenuItem);

        menuBar.add(fileMenu);

        editMenu.setText("Edit");

        copyMenuItem.setText("Copy");
        copyMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                copyMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(copyMenuItem);

        pasteMenuItem.setText("Paste");
        pasteMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                pasteMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(pasteMenuItem);

        cutMenuItem.setText("Cut");
        cutMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                cutMenuItemActionPerformed(evt);
            }
        });
        editMenu.add(cutMenuItem);

        menuBar.add(editMenu);

        compileMenu.setText("Compile");

        compileMenuItem.setText("Compile");
        compileMenuItem.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                compileMenuItemActionPerformed(evt);
            }
        });
        compileMenu.add(compileMenuItem);

        executeMenuItem.setText("Execute");
        compileMenu.add(executeMenuItem);

        menuBar.add(compileMenu);

        setJMenuBar(menuBar);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(toolBar, javax.swing.GroupLayout.DEFAULT_SIZE, 762, Short.MAX_VALUE)
            .addComponent(footPanel, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(editPanel)
                    .addComponent(outPanel))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(toolBar, javax.swing.GroupLayout.PREFERRED_SIZE, 25, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(editPanel, javax.swing.GroupLayout.DEFAULT_SIZE, 209, Short.MAX_VALUE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(outPanel, javax.swing.GroupLayout.PREFERRED_SIZE, 120, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addComponent(footPanel, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void saveMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveMenuItemActionPerformed
        saveFile();
    }//GEN-LAST:event_saveMenuItemActionPerformed

    private void updateStatus(int line, int column) {
        String text = String.valueOf(line) + "/" + String.valueOf(column);
        if (line == 0 && column == 0) {
            text = GUIMessages.APP_MESSAGE_NO_LINE;
        }

        lineLabel.setText(text);
    }

    private void editTextAreaFocusGained(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_editTextAreaFocusGained
        editTextArea.addCaretListener(new CaretListener() {
            @Override
            public void caretUpdate(CaretEvent ce) {
                int linenum = 1;
                int columnnum = 1;
                int caretpos = editTextArea.getCaretPosition();
                try {
                    linenum = editTextArea.getLineOfOffset(caretpos);
                    columnnum = caretpos - editTextArea.getLineStartOffset(linenum);
                    linenum += 1;
                    columnnum += 1;
                } catch (Exception e) {
                }

                updateStatus(linenum, columnnum);

            }
        });

    }//GEN-LAST:event_editTextAreaFocusGained

    private void editTextAreaFocusLost(java.awt.event.FocusEvent evt) {//GEN-FIRST:event_editTextAreaFocusLost
        editTextArea.removeCaretListener(editTextArea.getCaretListeners()[0]);
//        System.out.println("DDD " + editTextArea.getCaretListeners().length);
        updateStatus(0, 0);
    }//GEN-LAST:event_editTextAreaFocusLost

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing

        if (!this.file.isSaved()) {
            String ObjButtons[] = {GUIMessages.APP_MESSAGE_YES, GUIMessages.APP_MESSAGE_NO, GUIMessages.APP_MESSAGE_CANCEL};
            int PromptResult = JOptionPane.showOptionDialog(null, GUIMessages.DIALOG_SAVE_EXIT, GUIMessages.DIALOG_EXIT, JOptionPane.DEFAULT_OPTION, JOptionPane.WARNING_MESSAGE, null, ObjButtons, ObjButtons[1]);
            System.out.println(PromptResult);
            if (PromptResult == 1) {
                System.out.println("Exiting");
                System.exit(0);
            }

            if (PromptResult == 0) {
                if (saveAsFile()) {
                    System.exit(0);
                }
            }
        } else {
            System.out.println("Exiting");
            System.exit(0);
        }


    }//GEN-LAST:event_formWindowClosing

    private void editTextAreaKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_editTextAreaKeyPressed

    }//GEN-LAST:event_editTextAreaKeyPressed

    private void editTextAreaKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_editTextAreaKeyTyped

    }//GEN-LAST:event_editTextAreaKeyTyped

    private void editTextAreaKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_editTextAreaKeyReleased
        updateContent();
    }//GEN-LAST:event_editTextAreaKeyReleased

    private void compileMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_compileMenuItemActionPerformed
        compile();
    }//GEN-LAST:event_compileMenuItemActionPerformed

    private void saveAsMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveAsMenuItemActionPerformed
        saveAsFile();
    }//GEN-LAST:event_saveAsMenuItemActionPerformed

    private void openMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openMenuItemActionPerformed
       openFileAction();
    }//GEN-LAST:event_openMenuItemActionPerformed

    private void newMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_newMenuItemActionPerformed
        newFile();
    }//GEN-LAST:event_newMenuItemActionPerformed

    private void exitMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_exitMenuItemActionPerformed
        System.exit(0);
    }//GEN-LAST:event_exitMenuItemActionPerformed

    private void fileButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_fileButtonActionPerformed
        newFile();
    }//GEN-LAST:event_fileButtonActionPerformed

    private void openButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_openButtonActionPerformed
        openFileAction();
    }//GEN-LAST:event_openButtonActionPerformed

    private void saveButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_saveButtonActionPerformed
       saveFile();
    }//GEN-LAST:event_saveButtonActionPerformed

    private void copyButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyButtonActionPerformed
        copyToClipboard();
    }//GEN-LAST:event_copyButtonActionPerformed

    private void copyMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_copyMenuItemActionPerformed
       copyToClipboard();
    }//GEN-LAST:event_copyMenuItemActionPerformed

    private void pasteMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pasteMenuItemActionPerformed
        pasteFromClipboard();
    }//GEN-LAST:event_pasteMenuItemActionPerformed

    private void cutMenuItemActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cutMenuItemActionPerformed
        cutToClipboard();
    }//GEN-LAST:event_cutMenuItemActionPerformed

    private void cutButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_cutButtonActionPerformed
        cutToClipboard();
    }//GEN-LAST:event_cutButtonActionPerformed

    private void pasteButtonActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_pasteButtonActionPerformed
        pasteFromClipboard();
    }//GEN-LAST:event_pasteButtonActionPerformed


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JMenu compileMenu;
    private javax.swing.JMenuItem compileMenuItem;
    private javax.swing.JButton copyButton;
    private javax.swing.JMenuItem copyMenuItem;
    private javax.swing.JButton cutButton;
    private javax.swing.JMenuItem cutMenuItem;
    private javax.swing.JMenu editMenu;
    private javax.swing.JScrollPane editPanel;
    private javax.swing.JTextArea editTextArea;
    private javax.swing.JMenuItem executeMenuItem;
    private javax.swing.JMenuItem exitMenuItem;
    private javax.swing.JButton fileButton;
    private javax.swing.JMenu fileMenu;
    private javax.swing.JPanel footPanel;
    private javax.swing.JLabel lineLabel;
    private javax.swing.JMenuBar menuBar;
    private javax.swing.JMenuItem newMenuItem;
    private javax.swing.JButton openButton;
    private javax.swing.JMenuItem openMenuItem;
    private javax.swing.JScrollPane outPanel;
    private javax.swing.JTextArea outTextArea;
    private javax.swing.JButton pasteButton;
    private javax.swing.JMenuItem pasteMenuItem;
    private javax.swing.JMenuItem saveAsMenuItem;
    private javax.swing.JButton saveButton;
    private javax.swing.JMenuItem saveMenuItem;
    private javax.swing.JToolBar toolBar;
    private javax.swing.JToolBar.Separator toolBarSeparator;
    // End of variables declaration//GEN-END:variables
}
