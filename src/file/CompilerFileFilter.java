/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;
import java.io.File;
import java.util.regex.Pattern;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;

/**
 *
 * @author Edgar
 */
public class CompilerFileFilter extends FileFilter {
    CompilerFile file;
    
    public CompilerFileFilter(CompilerFile f){
        this.file = f;
    }
    
    //Accept all directories and all gif, jpg, tiff, or png files.
    public boolean accept(File f) {
        if (f.isDirectory()) {
            return true;
        }
        String fname = f.getName();
        String[] extensions = fname.split(Pattern.quote("."));
        if(extensions.length < 2) return false;
        String extension = extensions[1];
        if (extension != null) {
            if (extension.equals(this.file.getExtension())) {
                    return true;
            } else {
                return false;
            }
        }

        return false;
    }

    //The description of this filter
    public String getDescription() {
        return "*." + this.file.getExtension();
    }
}
