/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package file;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.CharBuffer;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.regex.Pattern;

/**
 *
 * @author Edgar
 */
public class CompilerFile {
    private File file;
    private String name;
    private String extension;
    private String path = "";
    private String content = "";
    private Boolean saved = true;
    private Boolean opened = false;

    public Boolean isOpened() {
        return opened;
    }
    
    
    public void clear(){
        this.content = "";
        this.path = "";
        this.name = "untitled";
        this.extension = "djt";
        this.saved = true;
        this.opened = false;
        this.file = new File(this.name + "." + this.extension);
    }

    public String getContent() {
        return content;
    }

    public File getFile() {
        return file;
    }

    // Do file stuff
    
    public String load() throws IOException {
        this.file = new File(this.path);
        this.opened = true;
        return readFile(this.path, StandardCharsets.UTF_8);

    }

    public void setSaved(Boolean saved) {
        this.saved = saved;
    }

    String readFile(String path, Charset encoding)
            throws IOException {
        byte[] encoded = Files.readAllBytes(Paths.get(path));
        return new String(encoded, encoding);
    }
    
    public CompilerFile(){
        name = "untitled";
        extension = "djt";
        this.file = new File(this.name + "." + this.extension);
    }
    
    public void addContent(String content){
        this.saved = false;
        this.content = content;
        System.out.println(this.content);
    }
    
    public Boolean save(){
        this.file = new File(this.path);
        
        System.out.println(this.file.getAbsoluteFile());
        try{
            BufferedWriter writer = new BufferedWriter(new FileWriter(this.file));
            writer.write(this.content);
            writer.close();
            this.saved = true;
            this.opened = true;
        }catch(Exception e){}
      
        System.out.println("Saving");
        return true;
    }

    public void setOpened(Boolean opened) {
        this.opened = opened;
    }

    public Boolean isSaved() {
        if(this.content.equals("")) return true;
        return saved;
    }

    public String getExtension() {
        return extension;
    }

    public void setExtension(String extension) {
        this.extension = extension;
    }

    public String getName() {

        return name;
    }

    public void setName(String name) {
        String[] filename = name.split(Pattern.quote("."));
        if(filename.length == 2){
            this.name = filename[0];
            this.extension = filename[1];
        }else{
            this.name = name;
        }
        
    }

    public String getPath() {
        return path;
    }

    public void setPath(String path) {
        if(path.contains(this.extension)){
            this.path = path;
        }else{
            this.path = path + "." + this.extension;
        }
        
    }
   
    
}
